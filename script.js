/* Global functions */
var mobileElementSwitcher = (function () {
    var hideClass = "tl__hide";
    var overflowClass = "tl__overflow";
    var activeClass = "tl__active";
    var body = $("body");
    var overlay = $("#main_overlay");

    var currentSwitcher = {
        switchButton: null,
        switchContainer: null,
        switchOn: null
    };

    var hideCurrentSwitcher = function () {
        if (currentSwitcher.switchContainer) {
            currentSwitcher.switchButton.removeClass(activeClass);
            currentSwitcher.switchContainer.addClass(hideClass);

            if (currentSwitcher.switchOffCallback) {
                currentSwitcher.switchOffCallback();
            }

        }
    };

    var setCurrentSwitcher = function (switchButton,switchContainer,options) {
        currentSwitcher.switchButton = switchButton;
        currentSwitcher.switchContainer = switchContainer;

        if (options.switchOffCallback) {
            currentSwitcher.switchOffCallback = options.switchOffCallback;
        }
    };

    var resetCurrentSwitcher = function () {
        currentSwitcher = {
            switchButton: null,
            switchContainer: null,
            switchOffCallback: null
        };
    };

    return function (options) {
        var switchButton = options.switchButton;
        var switchContainer = options.switchContainer;

        if (!switchButton.length || !switchContainer.length) {
            return;
        }

        switchButton.on("click", function (event) {

            var iterator = 0;
            var target = $(event.target);

            while (true) {

                if (target.attr("id") == switchButton.attr("id")) {
                    break;
                }

                if (target.attr("id") == switchContainer.attr("id")) {
                    return;
                }

                if (iterator >= 10) {
                    break;
                }

                target = target.parent();
                iterator++;
            }

            if (switchContainer.is(":visible")) {

                switchButton.removeClass(activeClass);
                body.removeClass(overflowClass);
                switchContainer.addClass(hideClass);

                if (overlay.length) {
                    overlay.addClass(hideClass);
                }

                if (options.switchOffCallback) {
                    options.switchOffCallback();
                }
                resetCurrentSwitcher();

            } else {
                hideCurrentSwitcher();

                switchButton.addClass(activeClass);
                body.addClass(overflowClass);
                switchContainer.removeClass(hideClass);

                if (overlay.length) {
                    overlay.removeClass(hideClass);
                }

                if (options.switchOnCallback) {
                    options.switchOnCallback();
                }

                setCurrentSwitcher(switchButton,switchContainer,options);
            }
        });
    }

})();
/* End global functions */

/* Script for all pages */
(function () {

    /* Start functions */
    function mainMenuSwitcher (elements) {

        if (elements.hamburger && elements.mainMenuContainer) {

            var hamburgerElements = elements.hamburger.children();

            mobileElementSwitcher({
                switchButton: elements.hamburger,
                switchContainer: elements.mainMenuContainer,
                switchOnCallback: function () {
                    hamburgerElements.eq(0).addClass("tl__hide");
                    hamburgerElements.eq(1).removeClass("tl__hide");
                },
                switchOffCallback: function () {
                    hamburgerElements.eq(1).addClass("tl__hide");
                    hamburgerElements.eq(0).removeClass("tl__hide");
                }
            });

        }

    }

    function profileSwitcher (elements) {

        if (elements.profileLink && elements.profileContainer) {
            mobileElementSwitcher({
                switchButton: elements.profileLink,
                switchContainer: elements.profileContainer
            });
        }

        if (elements.signUpLink && elements.signUpContainer) {

            mobileElementSwitcher({
                switchButton: elements.signUpLink,
                switchContainer: elements.signUpContainer
            });
        }

        if (elements.signInLink && elements.signInContainer) {
            mobileElementSwitcher({
                switchButton: elements.signInLink,
                switchContainer: elements.signInContainer
            });
        }
    }

    function footerColumnSlide () {
        var column = $(".tl__footer__item");

        if (!column.length) {
            return;
        }

        var showClass = "tl__footer__column__content__show";

        column.on("click", function (event) {
            var target = $(event.target);

            if (target.hasClass("tl__footer__column__title")) {
                var content = target.parent().find(".tl__footer__column__content");

                if (content.hasClass(showClass)) {
                    content.removeClass(showClass);
                } else {
                    content.addClass(showClass);
                }
            }

        });
    }

    function pageUp () {
        var button = $("#page_up");

        if (!button.length) {
            console.log("button PAGE UP does not find!");
            return;
        }

        var dom = $('html');

        button.on("click", function () {
            dom.animate({'scrollTop':0},800);
        });

    }
    /* End functions */


    /* Start code */
    var elements = {
        hamburger: $("#hamburger"),
        mainMenuContainer: $("#main_menu_container"),
        profileLink: $("#profile_link"),
        profileContainer: $("#profile_container"),
        signUpLink: $("#sign_up_link"),
        signUpContainer: $("#sign_up_container"),
        signInLink: $("#sign_in_link"),
        signInContainer: $("#sign_in_container")
    };

    mainMenuSwitcher(elements);
    profileSwitcher(elements);
    footerColumnSlide();
    pageUp();
    /* End code */
})();

/* End script for all pages */