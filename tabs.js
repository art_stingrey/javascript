function rpTabs (options) {
    if (!options.tabsContainerId) {
        return;
    }

    var tabMenu = $("#" + options.tabMenuId);
    var tabContainer = $("#" + options.tabsContainerId);
    var tabs = tabContainer.children();
    var menuItem = tabMenu.children();
    var activeTabIndex = 0;
    var obj = {};

    obj.showTab = function (index) {
        tabs.eq(activeTabIndex).addClass("rp__hide");
        tabs.eq(index).removeClass("rp__hide");
        activeTabIndex = index;
    };

    if (tabMenu && menuItem) {
        tabMenu.on("click", function (event) {
            var target = $(event.target);
            var iterator = 0;

            while (true) {
                if (target.is("li")) {
                    menuItem.eq(activeTabIndex).removeClass("active");
                    target.addClass("active");
                    var index = target.data("index");
                    obj.showTab(index);

                    if (options.tabSwitched) {
                        options.tabSwitched(activeTabIndex);
                    }

                    break;
                }

                if (iterator > 5) {
                    break;
                }

                iterator++;
                target = target.parent();
            }


        });
    }

    return obj;
}
